#!/bin/bash

# DEFAULT JVM SETTINGS (empty for automatic)
xmx="" # maximum RAM to be used: e.g. "-Xmx40g" for 40 GB
par="" # number of parallel processes to be spawned: e.g. "-p 16" for 16 processes

# OTHER SETTINGS
always_recompile="YES" # change if you do not want to recompile files in bin/ at every execution

### USAGE

function usage() {
    echo -e "\033[4mcommands and parameters:\033[0m"
    echo -e "    \033[1mall\033[0m:                             fully builds all experiments"
    echo -e "    \033[1mhelp\033[0m:                            show alchemist help"
    echo -e "    \033[1mgui\033[0m:                             run program in alchemist gui"
    echo -e "       <prog>"
    echo -e "    \033[1mcompile\033[0m:                         compile files into \"bin\" directory"
    echo -e "    \033[1mrun\033[0m:                             run batch tests, tracks and plots"
    echo -e "       <prog> <step> <endtime> <plots...> [regex] [variables]"
    echo -e "    \033[1mprogress\033[0m:                        track progress of a running process and plots"
    echo -e "       <prog> [points] <plots...> [regex]"
    echo -e "    \033[1mplot\033[0m:                            build resuming plots"
    echo -e "       <prog> [buckets] <plots...> [regex]"
    echo -e "    \033[1masy\033[0m:                             compile existing plots with asymptote"
    echo -e "       <file...>"
    echo -e "    \033[1mclean\033[0m:                           clean files produced by experiment"
    echo -e "       <prog>"
    exit 1
}

if [ "$1" == "" ]; then
    usage
fi
me="$0"
cmd="$1"
shift 1


### JAVA SETTINGS

if ! which javac >/dev/null 2>/dev/null; then
    javadir=`echo $HOME/alchemist/jdk*/bin`
    if [ -f "$javadir/javac" ]; then
        function java() {
            $javadir/java "$@"
        }
        function javac() {
            $javadir/javac "$@"
        }
    fi
fi
CP=`echo tools/alchemist-redist-*.jar`


### OTHER PROXY COMMANDS

if ! which free >/dev/null 2>/dev/null; then
    function free() {
        echo "Mem:          0           0"
        echo "Swap:         0           0"
    }
fi
if ! which pypy >/dev/null 2>/dev/null; then
    function pypy() {
        python "$@"
    }
fi
if ! which asy >/dev/null 2>/dev/null; then
    function asy() {
        echo asymptote missing: asy "$@"
    }
fi


### LOCAL COMMANDS: help gui compile clean

if [ "$cmd" == "help" ]; then
    if [ "$1" != "" ]; then
        usage
    fi
    java -cp "$CP" it.unibo.alchemist.Alchemist -h
    exit 0
fi

if [ "$cmd" == "gui" ]; then
    if [ "$1" == "" -o "$2" != "" ]; then
        usage
    fi
    bash "$me" compile
    prog="$1"
    java -cp "bin:$CP" it.unibo.alchemist.Alchemist -y src/main/yaml/"$prog".yml -g src/main/resources/"$prog".aes
    exit 0
fi

if [ "$cmd" = "compile" ]; then
    if [ "$1" != "" ]; then
        usage
    fi
    if [ $always_recompile == "YES" ]; then
        rm -rf bin
    fi
    if [ ! -d "bin" ]; then
        mkdir bin
    fi
    javac `find src/main/java/ -name "*.java"` -d bin/ -cp "$CP"
    function linker() {
        local dir=$1
        local mov=$2
        shift 2
        for res in "$@"; do
            bin="bin/${res#$dir}"
            if [ ! -e "$bin" ]; then
                ln -s "$mov/$res" "$bin"
            elif [ -d "$bin" ]; then
                if [ ! -L "$bin" ]; then
                    linker "$dir" "$mov/.." "$res"/*
                fi
            fi
        done
    }
    linker src/main/protelis  .. src/main/protelis/*
    linker src/main/resources .. src/main/resources/*
    exit 0
fi

if [ "$cmd" == "clean" ]; then
    if [ "$1" == "" -o "$2" != "" ]; then
        usage
    fi
    prog="$1"
    if [ ! -d TRASH ]; then
        mkdir TRASH
    fi
    for folder in "data/raw/$prog"?*; do
        name=`basename "$folder"`
        for ((k=0; k<1000; k++)); do
            if [ ! -e "TRASH/$name"v"$k" ]; then break; fi
        done
        mv "$folder" "TRASH/$name"v"$k"
    done
    mv "$prog"*.log TRASH/
    rm -f node*
    exit 0
fi


### REMOTE RUN

nodes=( )
while [[ "${1:0:1}${1%????}" =~ ^[0-9]+$ ]]; do
    nodes=("${nodes[@]}" "$1")
    shift 1
done
if [ "${#nodes[@]}" -eq 0 ]; then
    nodes=( 0 )
fi
node=$nodes

if which occam-run >/dev/null 2>/dev/null; then
    if [ "$cmd" == "all" -o "$cmd" == "progress" ]; then
        occam-run -n node$node sdonetti/alchemist bash "$me" $cmd $node "$@"
    else
        occam=`occam-run -n node$node sdonetti/alchemist bash "$me" $cmd $node "$@" | grep "Remote ID" | sed 's|.* ||'`
        while [ ! -f "$occam".log ]; do
            sleep 1
        done
        while [ ! -f "$occam".err ]; do
            sleep 1
        done
        tail -f "$occam".log &
        pidlog=$!
        tail -f "$occam".err &
        piderr=$!
        while [ ! -f "$occam".done ]; do
            sleep 1
        done
        rm "$occam".*
        exec 2> /dev/null
        kill $pidlog
        kill $piderr
    fi
    exit 0
elif [ -t 0 ]; then
    if [ "$cmd" == "all" -o "$cmd" == "progress" ]; then
        file="node$node-$RANDOM"
        nohup bash "$me" $cmd $node "$@" 1>>$file.log 2>>$file.err &
        #nohup bash -c "wait $!; echo JOB DONE > $file.done" 1>/dev/null 2>/dev/null &
        echo "Remote ID (file where output is stored): $file"
        exit 0
    fi
fi


### REMOTE COMMANDS: all run progress plot asy

if [ "$cmd" == "asy" ]; then
    if [ "$1" == "" ]; then
        usage
    fi
    cd data
    if [ ! -f "plot.asy" ]; then
        ln -s ../tools/plot.asy plot.asy
    fi
    for file in "$@"; do
        dir=`basename "$file" ".txt"`
        base=`echo "$dir" | sed 's|\.v[0-9]*$||'`
        if [ ! -f "$dir".txt ]; then
            echo "file $dir.txt not found, skipping..."
            continue
        fi
        if [ -e "$dir" ]; then
            echo -n "directory \"$dir\" exists: overwrite? (Y/n) "
            read x
            if [ "$x" == "n" -o "$x" == "N" ]; then
                continue
            fi
            rm -r "$dir"
        fi
        asy "$dir".txt -f pdf
        mkdir "$dir"
        mv "$base"*".pdf" "$dir"/
    done
    rm plot.asy
    exit 0
fi

if [ "$cmd" == "all" ]; then
    if [ "$1" != "" ]; then
        usage
    fi
    rm -rf data/
    echo "Screenshot..."
    bash "$me" gui isolation
    echo "Density Statistics..."
    bash "$me" run $node statistics 20 20 "rhol(rho)+rhol(nbr)+mean-rho(nbr)" 's|mean@rho|rho|g;s|rhol|num|g;s|"rho"|"$\\rho$"|g' random rhol
    bash "$me" progress $node statistics 1 | grep "^[ 0-9]*0%"
    pypy tools/statistics.py data/statistics.v0.txt
    bash "$me" asy $node data/statistics.v0.txt
    echo "Isolation Test..."
    bash "$me" run $node isolation 0.2 100 random ratel error
    bash "$me" progress $node isolation 100 | grep "^[ 0-9]*0%"
    bash "$me" run $node isolation 0.2 100 random ratel rhol
    bash "$me" progress $node isolation 100 | grep "^[ 0-9]*0%"
    bash "$me" plot $node isolation \
        "ratel:rhol=30&error=0.2&time(err)" \
        "ratel=0&rhol=30&time=4&error(err)+ratel=1&rhol=30&time=20&error(err)+ratel=2&rhol=30&time=100&error(err)" \
        "ratel=0&error=0.2&time=4&num(err)+ratel=1&error=0.2&time=20&num(err)+ratel=2&error=0.2&time=100&num(err)" \
        's|absolute ||g;s|"\([1-9]0\)"|"\1\\%"|g;s| error=0.2||g;s| rhol=30.0||g;s| time=[0-9]*.0||g;s|ratel=0[.0]*|$T=0.2$|g;s|ratel=1[.0]*|$T=1$|g;s|ratel=2[.0]*|$T=5$|g;s|plot.COLS = 3;|plot.COLS = 3; plot.ROWS = 3;|;s|plot;|plot; plot.styles = new pen[] {linetype(new real[] {1,2.5})+1, solid+1, solid+1, solid+1, solid+1, solid+1, linetype(new real[] {1,2.5})+1}; plot.colors = new pen[] {black, hsv(0,0.8,1), hsv(60,0.7,0.7), hsv(120,0.8,0.8), hsv(180,0.8,0.8), hsv(240,0.8,1), black};|;s|"isolation-timeerr-rat0|2.66, "isolation-timeerr-rat0|;s|"isolation-timeerr-rat1|13.7, "isolation-timeerr-rat1|;s|"isolation-timeerr-rat2|79.8, "isolation-timeerr-rat2|'
    echo "Done!"
    exit 0
fi


if [ "$1" == "" ]; then
    usage
fi
prog="$1"
fold="$prog$node"
path="data/raw/$fold"
files="$path/$prog"
shift 1


if [ "$cmd" == "plot" ]; then
    if [ "$1" == "" ]; then
        usage
    fi
    points="20"
    if [[ "$1" =~ ^[0-9]+$ ]]; then
	points="$1"
	shift 1
    fi
    plots=( )
    while [ "${1: -1}" == ")" ]; do
        plots=("${plots[@]}" "$1")
        shift 1
    done
    regex="$1"
    file=`ls "$files"* | head -n 1`
    for ((k=0; k<1000; k++)); do
        if [ ! -e "data/$prog.v$k.txt" ]; then break; fi
        if [ "$file" -ot "data/$prog.v$k.txt" ]; then break; fi
    done
    file="data/$prog.v$k.txt"
    python tools/plot_builder.py $points "$files"* "${plots[@]}" > "$file"
    if [ "$regex" != "" ]; then
        sed -e "$regex" -i "" "$file"
    fi
    bash "$me" asy $node "$file"
    exit 0
fi

if [ "$cmd" == "progress" ]; then
    points="10"
    if [[ "$1" =~ ^[0-9]+$ ]]; then
	points="$1"
	shift 1
    fi
    while [ "`echo "$files"*`" == "$files*" ]; do
        sleep 1
    done
    perc=100
    while [ ${perc:0:3} -eq 100 ]; do
        sleep 1
        perc=`python tools/plot_builder.py $points "$files"* 2>&1`
    done
    perc=0
    maxram=0
    maxswap=0
    totram=0
    totswap=0
    ram=0
    swap=0
    totfile=0
    while [ "${perc:0:3}" -lt 100 ]; do
        t="$perc"
        tf="$totfile"
        while [ "${t:0:3}" == "${perc:0:3}" -a "$tf" == "$totfile" ]; do
            sleep 2
            t=`python tools/plot_builder.py $points "$files"* 2>&1`
            tf=`echo $t | sed 's|[^/]*/||;s| .*||'`
        done
        perc="$t"
        totfile="$tf"
        MEM=`free -m | grep "Mem"  | tr -s ' '`
        SWP=`free -m | grep "Swap" | tr -s ' '`
        totram=` echo "$MEM" | cut -d ' ' -f 2`
        ram=`    echo "$MEM" | cut -d ' ' -f 3`
        totswap=`echo "$SWP" | cut -d ' ' -f 2`
        swap=`   echo "$SWP" | cut -d ' ' -f 3`
        totram=$[totram/1024]
        ram=$[ram/1024]
        maxram=$[maxram>ram?maxram:ram]
        maxswap=$[maxswap>swap?maxswap:swap]
        echo "$perc  (`date`)  $ram/$totram GB ram - $swap/$totswap MB swap"
    done
    if [ "$1" != "" ]; then
        bash "$me" plot "$node" "$prog" "$@"
    fi
    echo "FINISHED: $maxram/$totram GB ram - $maxswap/$totswap MB swap"
    exit 0
fi

if [ "$cmd" == "run" ]; then
    step="$1"
    time="$2"
    shift 2
    plots=( )
    while [ "${1: -1}" == ")" ]; do
        plots=("${plots[@]}" "$1")
        shift 1
    done
    if ! [[ "$1" =~ ^[a-z]*$ ]]; then
        plots=("${plots[@]}" "$1")
        shift 1
    fi
    if   [ "$node" == "12" ]; then
        xmx="-Xmx40g"
        par="-p 16"
    elif [ "$node" == "35" ]; then
        xmx="-Xmx512g"
        par="-p 32"
    fi
    if [ "$1" != "" -a "${1:0:1}" != "-" ]; then
        extra="-b -var"
    fi

    if [ ! -d "data" ]; then
        mkdir data
    fi
    if [ ! -d "data/raw" ]; then
        mkdir data/raw
    fi
    if [ ! -d "$path" ]; then
        mkdir "$path"
    else
        echo -n "files present in \"$path\": proceed anyway? (Y/n) "
        read x
        if [ "$x" == "n" -o "$x" == "N" ]; then
            exit 0
        fi
    fi
    if [ -f "$fold.log" ]; then
        echo -n "overwrite regular file $fold.log? (Y/n) "
        read x
        if [ "$x" != "n" -a "$x" != "N" ]; then
            echo -n > "$fold.log"
        fi
    fi
    if [ -f "$fold.err" ]; then
        echo -n "overwrite regular file $fold.err? (Y/n) "
        read x
        if [ "$x" != "n" -a "$x" != "N" ]; then
            echo -n > "$fold.err"
        fi
    fi

    bash "$me" compile
    nohup java $xmx -cp "bin:$CP" it.unibo.alchemist.Alchemist -y src/main/yaml/"$prog".yml -e "$files" $par -i "$step" -t "$time" $extra "$@" 1>>$fold.log 2>>$fold.err &
    points=`python -c "print int($time/$step)"`
    nohup bash "$me" progress "$nodes" "$prog" $points "${plots[@]}" 1>>$fold.log 2>>$fold.err &
    exit 0
fi

usage
