# Distributed Real-Time Shortest-Paths Computations with the Field Calculus

To appear in [RTSS 2018 - 39th IEEE Real-Time Systems Symposium](http://2018.rtss.org/). For any issues with reproducing the experiments, please contact [Giorgio Audrito](mailto:giorgio.audrito@gmail.com).


## Description

This repository contains all the source code used to perform the experiments presented in the paper.


## Prerequisites

* Importing the repository
    - Git

* Executing the simulations
    - Java 8 JDK installed and set as default virtual machine

* Chrunching the generated data and producing the charts
    - Python 2.7.x installed and set as default Python interpreter
    - [Asymptote 2.x](http://asymptote.sourceforge.net/) vector graphics compiler installed

* Code inspection and GUI execution
    - Eclipse Neon or Oxygen (older versions should work as well)

**Notes about the supported operating systems**

All the tests have been performed under Mac OS, CentOS Linux and Ubuntu Linux. No test has been performed under any other operating system, however, due to the portable nature of the used tools, the test suite should run on any Unix-like OSs.


## Reproducing the experiment


### Importing the repository

The first step is cloning this repository. It can be easily done by using `git`. Open a terminal, move it into an empty folder of your choice, then issue the following command:

``git clone https://gaudrito@bitbucket.org/gaudrito/rtss-experiment-potential-guarantees.git``

This should make all the required files appear in your repository, with the latest version.


### Executing the simulations

All the graphs for all simulations can be produced automatically by the command:

``./batch_tester.sh all``

issued from the root directory where the repository was cloned. The script is executed in background and saves output in the `data` folder as text files containing asymptote code, together with their compiled PDF versions.
In case of problems with the fully-automated procedure, or in case you want to vary some parameters, it is also possible to produce them step by step in the following way.

* First, compiled java class files (from `src/main/java/`) and a copy of all files in directory `src/main/protelis/` need to be present in directory `bin/`. If you already run graphical simulations with Eclipse (see the next section), they should already be present. Otherwise, you can produce them with:  
  ``./batch_tester.sh compile``
  
* Then, you can produce the raw data associated to the experiments with the following commands:  
  ``./batch_tester.sh run statistics 20 20 random rhol``  
  ``./batch_tester.sh run isolation 0.2 100 random ratel error``  
  ``./batch_tester.sh run isolation 0.2 100 random ratel rhol``  
  During execution, progress can be tracked through `log` files named as the experiment names. Raw results will be written in directory `data/raw/`.

* Finally, plots can be extracted with the following commands:  
  ``./batch_tester.sh plot statistics "rhol(rho)+rhol(nbr)+mean-rho(nbr)" 's|mean@rho|rho|g;s|rhol|num|g;s|"rho"|"$\\rho$"|g'``
  ``python tools/statistics.py data/statistics.v0.txt``
  ``./batch_tester.sh plot isolation "ratel:rhol=30&error=0.2&time(err)" "ratel=0&rhol=30&time=4&error(err)+ratel=1&rhol=30&time=20&error(err)+ratel=2&rhol=30&time=100&error(err)" "ratel=0&error=0.2&time=4&num(err)+ratel=1&error=0.2&time=20&num(err)+ratel=2&error=0.2&time=100&num(err)" 's|absolute ||g;s|"\([1-9]0\)"|"\1\\%"|g;s| error=0.2||g;s| rhol=30.0||g;s| time=[0-9]*.0||g;s|ratel=0[.0]*|$T=0.2$|g;s|ratel=1[.0]*|$T=1$|g;s|ratel=2[.0]*|$T=5$|g;s|plot.COLS = 3;|plot.COLS = 3; plot.ROWS = 3;|;s|plot;|plot; plot.styles = new pen[] {linetype(new real[] {1,2.5})+1, solid+1, solid+1, solid+1, solid+1, solid+1, linetype(new real[] {1,2.5})+1}; plot.colors = new pen[] {black, hsv(0,0.8,1), hsv(60,0.7,0.7), hsv(120,0.8,0.8), hsv(180,0.8,0.8), hsv(240,0.8,1), black};|;s|"isolation-timeerr-rat0|2.66, "isolation-timeerr-rat0|;s|"isolation-timeerr-rat1|13.7, "isolation-timeerr-rat1|;s|"isolation-timeerr-rat2|79.8, "isolation-timeerr-rat2|'`` 
  Results will be written in the `data/` directory as text files containing Asymptote source code able to produce the corresponding plots.

Different plots can be produced by varying the arguments of the `plot` commands. For a brief explanation of the plot descriptive syntax, issue the following command:

``tools/plot_builder.py``

* Lastly, Asymptote files can be compiled in PDF either manually (after copying `plot.asy` in the `data` folder) or by issuing the following command:  
  ``./batch_tester.sh asy data/*.txt``


### Inspecting the code and simulation GUI

Open Eclipse, click on "File > Import" and then on "Gradle > Existing Gradle Project", then select the folder of the repository donwloaded in the previous step.

To properly visualize the source files you should also install a Yaml editor (as `YEdit` in the Eclipse Marketplace) and the Protelis Parser through "Help > Install New Software" with the following address:

``http://efesto.apice.unibo.it/protelis-build/protelis-parser/protelis.parser.repository/target/repository/``

The files with the source code are in `src/main/protelis/` and contain the following:

* `utils.pt`: general utility functions
* `algorithm.pt`: distance estimation algorithm and metrics
* `statistics.pt`: statistic evaluation of density and neighbourhood size
* `isolation.pt`: distance estimation upon random walking in Vienna

The files describing the environment are in `src/main/yaml/` and contain the following:

* `statistics.yaml`: environment used for the statistic evaluation (Figure 5)
* `isolation.yaml`: environment used for the case study (Figures 4 and 6)

The two environments differ only for the set of parameters and exported values. In the `java` folder, there are files improving on the current Alchemist version (scheduled for future releases). In the `resources` folder, there are effect files for the GUI presentation together with GPS data. In order to run the simulations with a GUI, create a Run Configuration in Eclipse with the following settings.

* Main class: `it.unibo.alchemist.Alchemist`
* Program arguments: `-g src/main/resources/X.aes -y src/main/yaml/X.yml` where `X` can be either `statistics` or `isolation`

Finally, in  `tools/statistics.py` there is a small utility for adding interpolating plots to experiment results, used for the statistic evaluation of density and neighbourhood size. 
