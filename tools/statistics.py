#/usr/bin/env pypy

from math import *
from sys import *
from re import *

# deletes non-letter characters from a string
def sanitize(name):
    return sub(r'[^a-z]', '', name)

# computes the linear regression coefficients of a set of points
def linearegr(line):
    b = sum(p[1]/p[0] for p in line) / len(line)
    return 1, b # y = b x^1

# computes exponential regression coefficients of a set of points
def regression(line):
    xv = [log(p[0]) for p in line]
    yv = [log(p[1]) for p in line]
    ex = sum(xv) / len(xv)
    ey = sum(yv) / len(yv)
    vx = sum([(x - ex)**2 for x in xv])
    cv = sum([(xv[i] - ex)*(yv[i] - ey) for i in xrange(len(xv))])
    a  = 1 if vx == 0 else cv / vx
    if abs(a-round(a)) < 0.1:
        a = round(a)
    b = exp(ey - a*ex)
    return a, b # y = b x^a

# enhances a plot command by adding the line interpolation
def fixplot(s, square=False, linear=False):
    global numrho, numnbr
    
    head, code = s.split(' new pair[][] ')
    xvar, yvar = map(sanitize, head.split('", new string[]')[0].split('", "')[-2:])
    lines = head[:-2].split('new string[] {')[1].split(', ')
    code = eval(code[:-4].replace('{', '[').replace('}', ']'))
    if square:
        code = [[(p[0]**2,p[1]) for p in line] for line in code]
    n = len(code)
    comment = "// regression: "
    for i in xrange(n):
        a, b = (linearegr if linear else regression)(code[i])
        if (xvar, yvar) == ('rho', 'nbr'):
            a, b = numrho[0], numrho[1] * numnbr[i]
        code.append([(p[0], b*p[0]**a) for p in code[i]])
        comment += "%s %s = %.5f %s" % (lines[i][1:-1], yvar, b, xvar)
        if a != 1:
            comment += "^%.6f" % a
        comment += '  '
    if (xvar, yvar) == ('num', 'rho'):
        numrho = (1/a, (1/b) ** (1/a))
    if (xvar, yvar) == ('num', 'nbr'):
        numnbr = (linearegr(code[0])[1], linearegr(code[1])[1])
    lines = ', '.join([l[:-1] + ' (int)"' for l in lines])
    head = head[:-2] + ', ' + lines + '}, new pair[][] '
    return comment + '\n' + head + repr(code).replace('[', '{').replace(']', '}') + '));\n'

if len(argv) < 2:
    print "Missing argument (input file)."
    exit(0)

path = argv[1]
file = open(path).readlines()
file[7]  = fixplot(file[7], True)
file[9]  = fixplot(file[9], True, True)
file[11] = fixplot(file[11])
open(path, 'w').writelines(file)
